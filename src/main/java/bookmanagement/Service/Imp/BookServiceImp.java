package bookmanagement.Service.Imp;

import bookmanagement.Model.Book;
import bookmanagement.Page.Page;
import bookmanagement.Repository.BookRepository;
import bookmanagement.Repository.Provider.BookFilter;
import bookmanagement.Service.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BookServiceImp implements BookService {
    @Autowired
    private BookRepository bookRepository;

    @Override
    public boolean insert(Book book) {
        return bookRepository.insert(book);
    }

    @Override
    public boolean delete(int id) {
        return bookRepository.delete(id);
    }

    @Override
    public boolean update(Book book) {
        return bookRepository.update(book);
    }

    @Override
    public Book findById(int id) {
        return bookRepository.findById(id);
    }
    @Override
    public List<Book> findByFilter(BookFilter bookFilter,Page page){
        page.setTotalCount(bookRepository.countBooks(bookFilter));
        return bookRepository.findByFilter(bookFilter,page);
    }
}
