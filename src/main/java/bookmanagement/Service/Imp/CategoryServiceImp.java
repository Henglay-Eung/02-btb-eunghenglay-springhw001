package bookmanagement.Service.Imp;

import bookmanagement.Model.Category;
import bookmanagement.Page.Page;
import bookmanagement.Repository.CategoryRepository;
import bookmanagement.Service.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class CategoryServiceImp implements CategoryService {
    @Autowired
    private CategoryRepository categoryRepository;
    @Override
    public List<Category> findAll(String title,Page page) {
        page.setTotalCount(categoryRepository.countCategories(title));
        return categoryRepository.findAll(title,page);
    }

    @Override
    public boolean insert(Category category) {
        boolean status=categoryRepository.insert(category);
        return status;
    }

    @Override
    public boolean delete(int id) {
        return categoryRepository.delete(id);
    }

    @Override
    public boolean update( Category category) {
        return categoryRepository.update(category);
    }
}
