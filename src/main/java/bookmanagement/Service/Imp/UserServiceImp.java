package bookmanagement.Service.Imp;

import bookmanagement.Model.UserModel;
import bookmanagement.Repository.UserRepository;
import bookmanagement.Service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImp implements UserService {
    @Autowired
    private UserRepository userRepository;
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        UserModel userModel=userRepository.loadUserByUsername(username);
        userModel.setRoles(userRepository.selectRolesById(userModel.getId()));
        return userModel;
    }
}
