package bookmanagement.Service;

import bookmanagement.Model.Book;
import bookmanagement.Page.Page;
import bookmanagement.Repository.Provider.BookFilter;

import java.util.List;

public interface BookService {
    List<Book> findByFilter(BookFilter bookFilter, Page page);
    boolean insert(Book book);
    boolean delete(int id);
    boolean update(Book book);
    Book findById(int id);
}
