package bookmanagement.Service;

import bookmanagement.Model.Category;
import bookmanagement.Page.Page;

import java.util.List;

public interface CategoryService {
    List<Category> findAll(String title,Page page);
    boolean insert(Category category);
    boolean delete(int id);
    boolean update(Category category);
}
