package bookmanagement.Repository;

import bookmanagement.Model.Category;
import bookmanagement.Page.Page;
import bookmanagement.Repository.Provider.BookFilter;
import bookmanagement.Repository.Provider.CategoryProvider;
import bookmanagement.Repository.Provider.Provider;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.util.List;
@Repository
public interface CategoryRepository {
    @SelectProvider(value = CategoryProvider.class,method = "findByFilter")
    List<Category> findAll(@Param("title") String title,@Param("page") Page page);
    @Insert("INSERT INTO tb_categories (title) VALUES (#{title})")
    boolean insert(Category category);
    @Delete("DELETE FROM tb_categories WHERE id = #{id}")
    boolean delete(int id);
    @Update("UPDATE tb_categories SET (title) = #{title} WHERE id = #{id}")
    boolean update(Category category);
    @SelectProvider(value = CategoryProvider.class,method = "countCategories")
    int countCategories(String title);
}
