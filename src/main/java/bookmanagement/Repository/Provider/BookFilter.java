package bookmanagement.Repository.Provider;

public class BookFilter {
    private String title;
    private int category_id;
    public BookFilter(){

    }

    public BookFilter(String title, int category_id) {
        this.title = title;
        this.category_id = category_id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int  getCategory_id() {
        return category_id;
    }

    public void setCategory_id(int   category_id) {
        this.category_id = category_id;
    }
}
