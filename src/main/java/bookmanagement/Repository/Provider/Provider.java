package bookmanagement.Repository.Provider;

import bookmanagement.Page.Page;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.jdbc.SQL;

public class Provider {
    public String selectByFilter(@Param("filter") BookFilter bookFilter, @Param("page")Page page){
        return new SQL(){{
            SELECT("*");
            FROM("tb_books");
            if(bookFilter.getTitle()!=null)
                WHERE("title ILIKE '%' || #{filter.title} || '%'");
            if(bookFilter.getCategory_id()!=0)
                WHERE("category_id = #{filter.category_id}");
            LIMIT("#{page.limit}");
            OFFSET("#{page.offset}");
        }
        }.toString();
    }
    public String countBooks(BookFilter bookFilter){
        return new SQL(){{
           SELECT("COUNT(id)");
           FROM("tb_books");
            if(bookFilter.getTitle()!=null)
                WHERE("title ILIKE '%' || #{title} || '%'");
            if(bookFilter.getCategory_id()!=0)
                WHERE("category_id = #{category_id}");
        }}.toString();
    }
}
