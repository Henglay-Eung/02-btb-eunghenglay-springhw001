package bookmanagement.Repository.Provider;

import org.apache.ibatis.jdbc.SQL;

public class UserProvider {
    public String selectRolesById(int id){
        return new SQL(){{
            SELECT("r.id,r.name ");
            FROM ("roles r");
            INNER_JOIN("users_roles ur ON r.id= ur.role.id");
            WHERE("ur.user_id=#{id}");
        }}.toString();
    }
}
