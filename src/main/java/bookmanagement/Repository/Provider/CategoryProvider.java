package bookmanagement.Repository.Provider;

import bookmanagement.Page.Page;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.jdbc.SQL;

public class CategoryProvider {
    public String findByFilter(@Param("title") String title,@Param("page") Page page) {
        return new SQL() {
            {
                SELECT("*");
                FROM("tb_categories");
                if(title!=null)
                    WHERE("title ILIKE '%' || #{title} || '%'");
                LIMIT("#{page.limit}");
                OFFSET("#{page.offset}");
            }
        }.toString();
    }
    public String countCategories(String title){
        return new SQL(){{
            SELECT("COUNT(id)");
            FROM("tb_categories");
            if(title!=null)
                WHERE("title ILIKE '%' || #{title} || '%'");
        }}.toString();
    }
}
