package bookmanagement.Repository;


import bookmanagement.Model.Book;
import bookmanagement.Page.Page;
import bookmanagement.Repository.Provider.BookFilter;
import bookmanagement.Repository.Provider.Provider;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.util.List;
@Repository
public interface BookRepository {
    @Select("SELECT * FROM tb_books WHERE id = #{id}")
    Book findById(int id);
    @SelectProvider(value = Provider.class, method = "selectByFilter")
    List<Book> findByFilter(@Param("filter") BookFilter bookFilter, @Param("page") Page page);
    @SelectProvider(value = Provider.class,method = "countBooks")
    int countBooks(BookFilter bookFilter);
    @Insert("INSERT INTO tb_books (title,author,description,thumbnail,category_id) " +
            "VALUES (#{title},#{author},#{description},#{thumbnail},#{category_id})")
    boolean insert(Book book);
    @Delete("DELETE FROM tb_books WHERE id = #{id}")
    boolean delete(int id);
    @Update("UPDATE tb_books SET title=#{title}, author=#{author}, " +
            "description=#{description}, thumbnail=#{thumbnail}," +
            "category_id = #{category_id} WHERE id = #{id}")
    boolean update(Book book);
}
