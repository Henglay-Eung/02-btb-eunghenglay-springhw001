package bookmanagement.Repository;

import bookmanagement.Model.Role;
import bookmanagement.Model.UserModel;
import bookmanagement.Repository.Provider.UserProvider;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserRepository {
    @Select("SELECT * FROM  users WHERE username=#{username}")
    UserModel loadUserByUsername(String username);
    @Select("SELECT r.id,r.name from roles r\n"+
                "inner join users_roles ur on r.id=ur.role_id\n"+
                "where ur.user_id=#{id}")
    List<Role> selectRolesById(int id);
}
