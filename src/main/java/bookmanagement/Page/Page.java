package bookmanagement.Page;

public class Page {
    private int page;
    private int limit;
    private int nextPage;
    private int previousPage;
    private int totalCount;
    private int totalPages;
    private int pagesToShow;
    private int startPage;
    private int endPage;
    private int offset;
    public Page(){
        this.page=1;
        this.limit=10;
        this.totalCount=0;
        this.totalPages=0;
        this.pagesToShow=5;
    }
    public Page(int page,int limit,int totalCount,int totalPages,int pagesToShow){
        this.page=page;
        this.limit=limit;
        this.totalCount=totalCount;
        this.totalPages=totalPages;
        this.pagesToShow=pagesToShow;
    }
    public int getPage(){
        return this.page;
    }
    public void setPage(int page){
        if(page<=1)
            this.page=1;
        else
            this.page=page;
    }
    public int getTotalCount(){
        return this.totalCount;
    }
    public int getLimit(){
        return this.limit;
    }
    public void setLimit(int limit){
        this.limit=limit;
    }
    public int getTotalPages(){
        return (int) Math.ceil((double)this.totalCount/this.limit);
    }
    public void setTotalPages(int totalPages){
        this.totalPages=totalPages;
    }
    public void setStartPage(){
        this.startPage=1;
    }
    public int getNextPage(){
        return (this.page>=this.getTotalPages()?1:this.page+1);
    }
    public int getPreviousPage(){
        return (this.page<=1?1:this.page-1);
    }
    public int getOffset(){
        return (this.page-1)*this.limit;
    }
    public int getStartPage(){
        return this.startPage;
    }
    public int getEndPage(){
        return this.endPage;
    }
    public int getPagesToShow(){
        return this.pagesToShow;
    }
    public void setPagesToShow(int pagesToShow){
        this.pagesToShow=pagesToShow;
    }
    public void setTotalCount(int totalCount){
        this.totalCount=totalCount;
        this.setStartAndEndPage(this.getTotalPages());
    }
    public void setStartAndEndPage(int totalPages){
        int halfPage=pagesToShow/2;
        if(totalPages<=pagesToShow){
            this.startPage=1;
            this.endPage=totalPages;
        }
        else if(page-halfPage<=0){
            startPage=1;
            endPage=pagesToShow;
        }
        else if(page+halfPage==totalPages){
            startPage=page-halfPage;
            endPage=totalPages;
        }
        else if(page+halfPage>totalPages){
            startPage=totalPages-pagesToShow+1;
            endPage=totalPages;
        }
        else{
            startPage=page-halfPage;
            endPage=page+halfPage;
        }
    }
//    public void setTotalCount(int totalCount){
//        this.totalCount=totalCount;
//    }
//    public int getStartPage(){
//        return this.startPage;
//    }
//    public int getEndPage(){
//        return this.endPage;
//    }
//    public int getPagesToShow(){
//        return this.pagesToShow;
//    }
//    public int getOffset(){
//        return this.offset;
//    }
    @Override
    public String toString() {
        return "Page{" +
                "page=" + page +
                ", limit=" + limit +
                ", nextPage=" + nextPage +
                ", previousPage=" + previousPage +
                ", totalCount=" + totalCount +
                ", totalPages=" + totalPages +
                ", pagesToShow=" + pagesToShow +
                ", startPage=" + startPage +
                ", endPage=" + endPage +
                ", offset=" + offset +
                '}';
    }
}
