package bookmanagement.Model;

import bookmanagement.Page.Page;

import java.util.List;

public class ResponseModel<T> {
    private Page page;
    private T data;
    private String message;
    public ResponseModel(){

    }

    public ResponseModel(T data, Page page, String message) {
        this.page = page;
        this.data = data;
        this.message = message;
    }
    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public Page getPage() {
        return page;
    }

    public void setPage(Page page) {
        this.page = page;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
