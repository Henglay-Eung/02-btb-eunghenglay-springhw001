package bookmanagement.Model;

public class BookResponse {
    private Book data;
    private String message;
    private boolean status;
    public BookResponse(){

    }

    public BookResponse(Book data, String message, boolean status) {
        this.data = data;
        this.message = message;
        this.status = status;
    }

    public Book getData() {
        return data;
    }

    public void setData(Book data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }
}
