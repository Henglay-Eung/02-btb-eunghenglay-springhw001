package bookmanagement.Model;

public class FileResponse {
    private String data;
    private String message;
    private boolean status;
    public FileResponse(){

    }

    public FileResponse(String data, String message, boolean status) {
        this.data = data;
        this.message = message;
        this.status = status;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }
}
