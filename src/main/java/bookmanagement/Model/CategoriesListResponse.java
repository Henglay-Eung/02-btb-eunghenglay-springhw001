package bookmanagement.Model;

import bookmanagement.Page.Page;

import java.util.List;

public class CategoriesListResponse {
    private Page page;
    private List<Category> data;
    private String message;
    private boolean status;
    public CategoriesListResponse(){

    }

    public List<Category> getData() {
        return data;
    }

    public void setData(List<Category> data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public Page getPage() {
        return page;
    }

    public void setPage(Page page) {
        this.page = page;
    }

    public CategoriesListResponse(Page page, List<Category> data, String message, boolean status) {
        this.page=page;
        this.data = data;
        this.message = message;
        this.status = status;
    }
}
