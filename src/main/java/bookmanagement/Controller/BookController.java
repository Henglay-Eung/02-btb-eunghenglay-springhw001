package bookmanagement.Controller;

import bookmanagement.Model.Book;
import bookmanagement.Model.ResponseModel;
import bookmanagement.Model.BookResponse;
import bookmanagement.Page.Page;
import bookmanagement.Repository.Provider.BookFilter;
import bookmanagement.Service.Imp.BookServiceImp;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/books")
public class BookController {
    @Autowired
    private BookServiceImp bookServiceImp;
    @GetMapping()
    public ResponseEntity<ResponseModel> findAll(BookFilter bookFilter, Page page){
        List<Book> bookList=bookServiceImp.findByFilter(bookFilter,page);
        ResponseModel responseModel =new ResponseModel();
        if(bookList.isEmpty()){
            responseModel.setMessage("No data");

        }else {
            responseModel.setPage(page);
            responseModel.setData(bookList);
            responseModel.setMessage("OK");
        }
        return ResponseEntity.ok(responseModel);
    }
    @GetMapping("/{id}")
    public ResponseEntity<BookResponse> findById(@PathVariable int id){
        BookResponse bookResponse = new BookResponse();
        bookResponse.setData(bookServiceImp.findById(id));
        bookResponse.setMessage("success");
        bookResponse.setStatus(true);
        return ResponseEntity.ok(bookResponse);
    }
    @PostMapping()
    public ResponseEntity<String> insert(@RequestBody Book book){
        String message="OK";
        bookServiceImp.insert(book);
        return ResponseEntity.ok(message);
    }
    @DeleteMapping("/{id}")
    public ResponseEntity<String> delete(@PathVariable int id){
        String message="OK";
        bookServiceImp.delete(id);
        return ResponseEntity.ok(message);
    }
    @PatchMapping()
    public ResponseEntity<String> update(@RequestBody Book book){
        String message="OK";
        bookServiceImp.update(book);
        return ResponseEntity.ok(message);
    }
}
