package bookmanagement.Controller;

import bookmanagement.Model.FileResponse;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.UUID;

@RestController
@RequestMapping()
public class FileController {
    @PostMapping("/upload")
    public ResponseEntity<FileResponse> uploadFile(@RequestParam("file") MultipartFile file) throws IOException {
        File receivedFile = new File("src/main/java/bookmanagement/Image/"+ UUID.randomUUID() +file.getOriginalFilename());
        receivedFile.createNewFile();
        FileOutputStream fout= new FileOutputStream(receivedFile);
        fout.write(file.getBytes());
        fout.close();
        FileResponse fileResponse=new FileResponse();
        fileResponse.setData("http://localhost:8080/image/"+receivedFile.getName());
        fileResponse.setMessage("Uploaded the file successfully");
        fileResponse.setStatus(true);
        return ResponseEntity.ok(fileResponse);
    }
    
}
