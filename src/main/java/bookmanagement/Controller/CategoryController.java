package bookmanagement.Controller;

import bookmanagement.Model.CategoriesListResponse;
import bookmanagement.Model.Category;
import bookmanagement.Model.ResponseModel;
import bookmanagement.Page.Page;
import bookmanagement.Service.Imp.CategoryServiceImp;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/categories")
public class CategoryController {
    @Autowired
    private CategoryServiceImp categoryServiceImp;
    @GetMapping
    public ResponseEntity<ResponseModel<List<Category>>> findAll(String title, Page page){
        ResponseModel<List<Category>> responseModel= new ResponseModel<List<Category>>();
        responseModel.setPage(page);
        responseModel.setData(categoryServiceImp.findAll(title,page));
        responseModel.setMessage("OK");
        return ResponseEntity.ok(responseModel);
    }
    @PostMapping
    public ResponseEntity<String> insert(@RequestBody Category category){
        String message="OK";
        categoryServiceImp.insert(category);
        return ResponseEntity.ok(message);
    }
    @DeleteMapping("/{id}")
    public ResponseEntity<String> delete(@PathVariable int id){
        String message="OK";
        categoryServiceImp.delete(id);
        return ResponseEntity.ok(message);
    }
    @PatchMapping()
    public ResponseEntity<String> update(@RequestBody Category category){
        String message="OK";
        categoryServiceImp.update(category);
        return ResponseEntity.ok(message);
    }
}
